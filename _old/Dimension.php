<?php

/**
 * Jantia
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//

//
use Jantia\Stdlib\Exception\BadMethodCallException;

/**
 * Slowly Changing Dimensions (SCD) dimension trait.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait Dimension {
	
	/**
	 * SCD dimension
	 *
	 * @var DimensionType
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionType $_dimensionType;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function updateAllowed() : bool {
		// Return true if dimension type is allowed for update
		if(! empty($this->_dimensionType)):
			return ( ! in_array($this->getDimensionType()->value, [0, 2, 4]) );
		else:
			throw new BadMethodCallException('Dimension type value is not set.');
		endif;
	}
	
	/**
	 * @return null|DimensionType
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionType() : ?DimensionType {
		return $this->_dimensionType ?? NULL;
	}
	
	/**
	 * @param    DimensionType    $value
	 *
	 * @return Dimension
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionType(DimensionType $value) : static {
		//
		$this->_dimensionType = $value;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function insertRequired() : bool {
		// Return true if dimension type is allowed for insert
		if(! empty($this->_dimensionType)):
			return ( in_array($this->getDimensionType()->value, [0, 2, 4, 6]) );
		else:
			throw new BadMethodCallException('Dimension type value is not set.');
		endif;
	}
}

<?php

/**
 * Jantia
 *
 * @package        Jantia/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Slowly Changing Dimensions (SCD) dimension type's enumeration.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionType: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * No changes are tracked. The data remains static.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case FIXED = 0;
	
	/**
	 * Overwrite the old data with the new data. Historical data is lost.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case NO_HISTORY = 1;
	
	/**
	 * Track historical data by creating new records with versioning or effective date columns. This allows for historical analysis.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case ROW_VERSION = 2;
	
	/**
	 * Track changes using additional columns to store previous values. This is less commonly used compared to Type 2.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PREVIOUS_VALUE = 3;
	
	/**
	 * Track history in a separate history table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HISTORY = 4;
	
	/**
	 * A hybrid approach combining features of Types 1, 2, and 3.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HYBRID = 6;
}

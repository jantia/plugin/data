<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Role;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionRoleInterface {
	
	/**
	 * Checks if the role-playing dimension is used as an order date in the fact table. Role-playing dimensions
	 * are reused in different contexts. Returns true if the dimension is acting as an order date.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isOrderDate() : bool;
	
	/**
	 * Retrieves the current role-playing dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionRole
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionRole() : ?DimensionRole;
	
	/**
	 * Sets the role-playing dimension type and returns the instance for method chaining.
	 *
	 * @param    DimensionRole    $value
	 *
	 * @return DimensionRoleInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionRole(DimensionRole $value) : DimensionRoleInterface;
}
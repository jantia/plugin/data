<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Role;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Role-Playing Dimensions</h6><br>
 * <b>Purpose:</b> A dimension that plays multiple roles within the same fact table. For example, a date dimension
 * could serve as both "Order Date" and "Shipping Date."<br><br>
 * <b>CQRS Use:</b> In CQRS, the Command side would store different roles for the same dimension (e.g., Order Date,
 * Shipping Date), and the Query side would need to retrieve data based on the specific role.<br><br>
 * <b>Example:</b> Date dimension acting as both the purchase date and the delivery date.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionRole: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * A role-playing dimension used as the order date in a fact table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case ORDER_DATE = 0;
	
	/**
	 * A role-playing dimension used as the shipping date in a fact table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case SHIPPING_DATE = 1;
	
	/**
	 * A role-playing dimension used as the delivery date, typically for tracking when an order was received.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case DELIVERY_DATE = 2;
	
	/**
	 * A role-playing dimension representing the payment date, indicating when a transaction was paid.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PAYMENT_DATE = 3;
	
	/**
	 * A role-playing dimension representing an employee's hire date in a fact table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HIRE_DATE = 4;
	
	/**
	 * A role-playing dimension used for contract start date in a fact table to track contract-related timelines.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case CONTRACT_START_DATE = 5;
}
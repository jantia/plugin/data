<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Role;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     DimensionRoleInterface
 */
class DimensionRolePlugin implements DimensionRoleInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TraitDimensionRole;
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Role;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * Role-Playing Dimension trait.
 *  Role-playing dimensions are reused in different contexts within a fact table,
 *  such as a date dimension used as both order and delivery date.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionRole {
	
	/**
	 * Role-playing dimension type.
	 *
	 * @var DimensionRole
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionRole $_dimensionRole;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isOrderDate() : bool {
		// Return true if the dimension is used as an order date.
		if(! empty($this->_dimensionRole)):
			return ( $this->getDimensionRole()->value === DimensionRole::ORDER_DATE->value );
		endif;
		
		//
		throw new BadMethodCallException('Role-playing dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionRole
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionRole() : ?DimensionRole {
		return $this->_dimensionRole ?? NULL;
	}
	
	/**
	 * @param    DimensionRole    $value
	 *
	 * @return DimensionRoleInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionRole(DimensionRole $value) : DimensionRoleInterface {
		//
		$this->_dimensionRole = $value;
		
		//
		return $this;
	}
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Rapidly;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * <h6>Rapidly Changing Dimension trait.</h6>
 * Rapidly changing dimensions update frequently, such as customer behavior,
 * financial data, or product details, and require efficient handling.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionRapidly {
	
	/**
	 * Rapidly changing dimension type.
	 *
	 * @var DimensionRapidly
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionRapidly $_dimensionRapidly;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isFrequentlyUpdated() : bool {
		// Return true if the dimension is updated frequently.
		if(! empty($this->_dimensionRapidly)):
			return ( $this->getDimensionRapidly()->value === DimensionRapidly::REAL_TIME_TRACKING->value );
		endif;
		
		//
		throw new BadMethodCallException('Rapidly changing dimension type is not set.');
	}
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionRapidly() : ?DimensionRapidly {
		return $this->_dimensionRapidly ?? NULL;
	}
	
	/**
	 * @param    DimensionRapidly    $value
	 *
	 * @return DimensionRapidlyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionRapidly(DimensionRapidly $value) : DimensionRapidlyInterface {
		//
		$this->_dimensionRapidly = $value;
		
		//
		return $this;
	}
}
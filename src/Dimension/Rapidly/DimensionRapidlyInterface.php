<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Rapidly;

/**
 * <h6>Rapidly Changing Dimension trait.</h6>
 *  Rapidly changing dimensions update frequently, such as customer behavior,
 *  financial data, or product details, and require efficient handling.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionRapidlyInterface {
	
	/**
	 * Checks if the rapidly changing dimension is frequently updated, such as real-time tracking data, financial
	 * updates, or customer interactions. Returns true if the dimension is subject to frequent updates.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isFrequentlyUpdated() : bool;
	
	/**
	 * Retrieves the current rapidly changing dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionRapidly
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionRapidly() : ?DimensionRapidly;
	
	/**
	 * Sets the rapidly changing dimension type and returns the updated instance for method chaining.
	 *
	 * @param    DimensionRapidly    $value
	 *
	 * @return DimensionRapidlyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionRapidly(DimensionRapidly $value) : DimensionRapidlyInterface;
}
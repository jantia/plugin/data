<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Rapidly;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Rapidly Changing Dimensions</h6><br>
 * <b>Purpose:</b> Handle dimensions that change frequently, often faster than SCD can manage efficiently. These dimensions
 * can become very large if treated as SCD Type 2 (adding new records for every change).<br><br>
 * <b>CQRS Use:</b> In CQRS, the Command side would process frequent updates, and the Query side would be optimized to
 * handle large amounts of historical data. Often, specialized query mechanisms like materialized views or dedicated
 * caching may be employed to speed up queries.<br><br>
 * <b>Example:</b> Stock prices or frequent customer behavior updates.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionRapidly: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * A rapidly changing dimension where the data changes frequently, such as real-time tracking of user behavior or sensor data.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case REAL_TIME_TRACKING = 0;
	
	/**
	 * A rapidly changing dimension that stores volatile attributes, such as rapidly updating financial data or stock prices.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case VOLATILE_ATTRIBUTES = 1;
	
	/**
	 * A rapidly changing dimension that handles frequent updates to customer interactions, such as page views, clicks, or logins.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case CUSTOMER_INTERACTIONS = 2;
	
	/**
	 * A rapidly changing dimension where product details (e.g., prices, stock availability) are updated frequently.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PRODUCT_UPDATES = 3;
	
	/**
	 * A rapidly changing dimension that deals with frequent updates to social media activity, such as likes, comments, or shares.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case SOCIAL_MEDIA_ACTIVITY = 4;
}
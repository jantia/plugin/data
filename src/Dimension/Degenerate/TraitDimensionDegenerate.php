<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Degenerate;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * <h6>Degenerate Dimension trait.</h6>
 * Degenerate dimensions are attributes stored directly in fact tables,
 * often as transactional identifiers like invoice numbers or order numbers.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionDegenerate {
	
	/**
	 * Degenerate dimension type.
	 *
	 * @var DimensionDegenerate
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionDegenerate $_dimensionDegenerate;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isTransactionalReference() : bool {
		// Return true if the dimension is a transactional reference.
		if(! empty($this->_dimensionDegenerate)):
			return ( $this->getDimensionDegenerate()->value === DimensionDegenerate::TRANSACTIONAL_REFERENCE->value );
		endif;
		
		//
		throw new BadMethodCallException('Degenerate dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionDegenerate
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionDegenerate() : ?DimensionDegenerate {
		return $this->_dimensionDegenerate ?? NULL;
	}
	
	/**
	 * @param    DimensionDegenerate    $value
	 *
	 * @return DimensionDegenerateInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionDegenerate(DimensionDegenerate $value) : DimensionDegenerateInterface {
		//
		$this->_dimensionDegenerate = $value;
		
		//
		return $this;
	}
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Degenerate;

/**
 * <h6>Degenerate Dimension trait.</h6>
 *  Degenerate dimensions are attributes stored directly in fact tables,
 *  often as transactional identifiers like invoice numbers or order numbers.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionDegenerateInterface {
	
	/**
	 * Checks if the dimension represents a transactional reference, such as an invoice number or order number,
	 * that is stored directly in the fact table without its own dimension table. Returns true if the dimension is a
	 * transactional reference.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isTransactionalReference() : bool;
	
	/**
	 * Retrieves the current degenerate dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionDegenerate
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionDegenerate() : ?DimensionDegenerate;
	
	/**
	 * Sets the degenerate dimension type and returns the instance for method chaining.
	 *
	 * @param    DimensionDegenerate    $value
	 *
	 * @return DimensionDegenerateInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionDegenerate(DimensionDegenerate $value) : DimensionDegenerateInterface;
}
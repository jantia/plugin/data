<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Degenerate;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Denegerate Dimensions</h6><br>
 * <b>Purpose:</b> A dimension that is stored in the fact table, usually as an identifier or transactional attribute,
 * but doesn’t have its own dimension table.<br><br>
 * <b>CQRS Use:</b> In CQRS, the Command side might issue a new transaction with these degenerate dimension values
 * (e.g., invoice numbers). The Query side retrieves or aggregates data based on these transaction-level
 * identifiers.<br><br>
 * <b>Example:</b> Order numbers or invoice numbers stored in the fact table without a corresponding dimension.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionDegenerate: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * A degenerate dimension where the attribute is directly stored in the fact table, typically a transactional reference (e.g., an invoice number).
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case TRANSACTIONAL_REFERENCE = 0;
	
	/**
	 * A degenerate dimension that contains identification details like order numbers or IDs used across multiple fact tables.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case IDENTIFICATION_DETAIL = 1;
	
	/**
	 * A degenerate dimension where the fact table stores summarized or aggregated data, without storing details in a separate dimension table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case AGGREGATED_FACT = 2;
	
	/**
	 * A degenerate dimension used in high-performance queries where storing a dimension table would add unnecessary complexity.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PERFORMANCE_OPTIMIZED = 3;
	
	/**
	 * A degenerate dimension that tracks historical transactional references for auditing purposes, but is still stored within the fact table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HISTORICAL_REFERENCE = 4;
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Inferred;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * <h6>Inferred Dimension trait.</h6>
 * Inferred dimensions are placeholders when the fact data arrives
 * before the complete dimension data is available.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionInferred {
	
	/**
	 * Inferred dimension type.
	 *
	 * @var DimensionInferred
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionInferred $_dimensionInferred;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isPlaceholder() : bool {
		// Return true if the dimension is a placeholder.
		if(! empty($this->_dimensionInferred)):
			return ( $this->getDimensionInferred()->value === DimensionInferred::PLACEHOLDER->value );
		endif;
		
		//
		throw new BadMethodCallException('Inferred dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionInferred
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionInferred() : ?DimensionInferred {
		return $this->_dimensionInferred ?? NULL;
	}
	
	/**
	 * @param    DimensionInferred    $value
	 *
	 * @return DimensionInferredInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionInferred(DimensionInferred $value) : DimensionInferredInterface {
		//
		$this->_dimensionInferred = $value;
		
		//
		return $this;
	}
}
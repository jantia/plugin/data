<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Inferred;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Inferred Dimensions</h6><br>
 * <b>Purpose:</b> Placeholder dimensions that are used temporarily when a fact arrives before the complete
 * dimensional data is available. Once the dimensional data arrives, it is updated.<br><br>
 * <b>CQRS Use:</b> Inferred dimensions fit well with CQRS because the Command side can initially create or update
 * records with partial or placeholder data, allowing the system to continue processing while awaiting the final
 * dimensional information. The Query side will then retrieve and display either the inferred (temporary) or final
 * data based on what is available. When the complete dimensional data arrives, the Command side updates the inferred
 * dimension, and the Query side reflects the new, accurate data.<br><br>
 * <b>Example:</b> When an order is placed in an e-commerce system but the full customer information isn't available
 * yet (e.g., because it's awaiting confirmation from an external system), an inferred dimension can hold a
 * placeholder for the customer. Later, when the full customer details are confirmed, the system updates the inferred
 * customer record with the actual data.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionInferred: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * The inferred dimension is created as a placeholder when the full dimension data is not yet available.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PLACEHOLDER = 0;
	
	/**
	 * The inferred dimension is updated later with the full and accurate data once it becomes available.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case UPDATED = 1;
	
	/**
	 * The inferred dimension has partial data, meaning some attributes are filled in, while others are still missing.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PARTIAL = 2;
	
	/**
	 * The inferred dimension is used temporarily but may be replaced or overwritten by more accurate data later.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case TEMPORARY = 3;
	
	/**
	 * The inferred dimension remains incomplete and may never be updated, but it still serves a purpose in the fact table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case INCOMPLETE = 4;
}
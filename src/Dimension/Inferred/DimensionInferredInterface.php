<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Inferred;

/**
 * <h6>Inferred Dimension trait.</h6>
 * Inferred dimensions are placeholders when the fact data arrives
 * before the complete dimension data is available.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionInferredInterface {
	
	/**
	 * Checks if the current inferred dimension is a placeholder used temporarily when the full dimension data is
	 * not available. Returns true if the dimension is a placeholder.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isPlaceholder() : bool;
	
	/**
	 * Retrieves the current inferred dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionInferred
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionInferred() : ?DimensionInferred;
	
	/**
	 * Sets the inferred dimension type and returns the updated instance for method chaining.
	 *
	 * @param    DimensionInferred    $value
	 *
	 * @return DimensionInferredInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionInferred(DimensionInferred $value) : DimensionInferredInterface;
}
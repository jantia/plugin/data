<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Slowly;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

use function in_array;

/**
 * <h6>Slowly Changing Dimension (SCD) trait.</h6>
 * Slowly changing dimensions track changes over time, and different types
 * allow for varying levels of historical tracking (e.g., overwriting, versioning).
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionSlowly {
	
	/**
	 * Slowly changing dimension type.
	 *
	 * @var DimensionSlowly
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionSlowly $_dimensionSlowly;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isHistoryTracked() : bool {
		// Return true if history is tracked for the dimension.
		if(! empty($this->_dimensionSlowly)):
			// Check if the dimension type is one of the allowed types for tracking history.
			return in_array($this->getDimensionSlowly()->value, [DimensionSlowly::ROW_VERSION->value,
			                                                     DimensionSlowly::PREVIOUS_VALUE->value,
			                                                     DimensionSlowly::HISTORY->value], TRUE);
		endif;
		
		// If the dimension type is not set, throw an exception.
		throw new BadMethodCallException('Slowly changing dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionSlowly
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionSlowly() : ?DimensionSlowly {
		return $this->_dimensionSlowly ?? NULL;
	}
	
	/**
	 * @param    DimensionSlowly    $value
	 *
	 * @return DimensionSlowlyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionSlowly(DimensionSlowly $value) : DimensionSlowlyInterface {
		//
		$this->_dimensionSlowly = $value;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isOverwriteAllowed() : bool {
		// Return true if overwriting the dimension is allowed (no history tracked).
		if(! empty($this->_dimensionSlowly)):
			return ( $this->getDimensionSlowly()->value === DimensionSlowly::NO_HISTORY->value );
		endif;
		
		// Throw an exception if the dimension type is not set.
		throw new BadMethodCallException('Slowly changing dimension type is not set.');
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function requiresNewRowForChanges() : bool {
		// Return true if changes require a new row or versioning for history tracking.
		if(! empty($this->_dimensionSlowly)):
			// Check if the dimension type is one of the allowed types for tracking history.
			return in_array($this->getDimensionSlowly()->value,
			                [DimensionSlowly::ROW_VERSION->value, DimensionSlowly::HISTORY->value], TRUE);
		endif;
		
		// Throw an exception if the dimension type is not set.
		throw new BadMethodCallException('Slowly changing dimension type is not set.');
	}
}
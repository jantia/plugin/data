<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Slowly;

/**
 * <h6>Slowly Changing Dimensions (SCD)</h6><br>
 *  <b>Purpose:</b> Track the changes in data over time, typically for non-volatile data such as customer information,
 *  product details, or employee records.<br><br>
 *  <b>CQRS Use:</b> SCD fits well with CQRS because the Command side handles the changes (e.g., updating user info),
 *  and the Query side retrieves different historical versions of the data.<br><br>
 *  <b>Types of SCD:</b>
 *  <ul>
 *  <li><b>SCD Type 1:</b> Overwrites data without keeping historical information.</li>
 *  <li><b>SCD Type 2:</b> Keeps historical data by adding new records for changes.</li>
 *  <li><b>SCD Type 3:</b> Tracks limited historical information by adding new columns for changes.</li>
 *  <li><b>SCD Type 4:</b> Track history in a separate history table.</li>
 *  <li><b>SCD Hybrid:</b> Combines elements of multiple types (Type 1, Type 2, and Type 3).</li>
 *  </ul>
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionSlowlyInterface {
	
	/**
	 * Checks if the slowly changing dimension tracks historical data. Returns true if the dimension type is set to
	 * maintain historical records (Type 2, Type 3, or using a separate history table).
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isHistoryTracked() : bool;
	
	/**
	 * Retrieves the current slowly changing dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionSlowly
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionSlowly() : ?DimensionSlowly;
	
	/**
	 * Sets the slowly changing dimension type and returns the updated instance for method chaining.
	 *
	 * @param    DimensionSlowly    $value
	 *
	 * @return DimensionSlowlyInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionSlowly(DimensionSlowly $value) : DimensionSlowlyInterface;
	
	/**
	 * Checks if the dimension type allows overwriting of data without tracking history. Returns true if
	 * overwriting is permitted (Type 1, where no history is kept).
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isOverwriteAllowed() : bool;
	
	/**
	 * Determines if the dimension type requires a new row or versioning when changes occur to track history.
	 * Returns true for dimensions that need new records for every change (Type 2 or Type 4).
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function requiresNewRowForChanges() : bool;
}
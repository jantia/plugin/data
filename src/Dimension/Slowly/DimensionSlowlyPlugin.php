<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Slowly;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     DimensionSlowlyInterface
 */
class DimensionSlowlyPlugin implements DimensionSlowlyInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TraitDimensionSlowly;
}
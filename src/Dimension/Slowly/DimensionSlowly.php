<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Slowly;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Slowly Changing Dimensions (SCD)</h6><br>
 * <b>Purpose:</b> Track the changes in data over time, typically for non-volatile data such as customer information,
 * product details, or employee records.<br><br>
 * <b>CQRS Use:</b> SCD fits well with CQRS because the Command side handles the changes (e.g., updating user info),
 * and the Query side retrieves different historical versions of the data.<br><br>
 * <b>Types of SCD:</b>
 * <ul>
 * <li><b>SCD Type 1:</b> Overwrites data without keeping historical information.</li>
 * <li><b>SCD Type 2:</b> Keeps historical data by adding new records for changes.</li>
 * <li><b>SCD Type 3:</b> Tracks limited historical information by adding new columns for changes.</li>
 * <li><b>SCD Type 4:</b> Track history in a separate history table.</li>
 * <li><b>SCD Hybrid:</b> Combines elements of multiple types (Type 1, Type 2, and Type 3).</li>
 * </ul>
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionSlowly: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * No changes are tracked. The data remains static.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case FIXED = 0;
	
	/**
	 * Overwrite the old data with the new data. Historical data is lost.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case NO_HISTORY = 1;
	
	/**
	 * Track historical data by creating new records with versioning or effective date columns. This allows for historical analysis.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case ROW_VERSION = 2;
	
	/**
	 * Track changes using additional columns to store previous values. This is less commonly used compared to Type 2.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PREVIOUS_VALUE = 3;
	
	/**
	 * Track history in a separate history table.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HISTORY = 4;
	
	/**
	 * A hybrid approach combining features of Types 1, 2, and 3.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HYBRID = 6;
}
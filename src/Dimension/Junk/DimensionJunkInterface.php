<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Junk;

/**
 * <h6>Junk Dimension trait.</h6>
 *  Junk dimensions consolidate multiple low-cardinality attributes,
 *  such as Boolean flags or small categories, into a single dimension.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionJunkInterface {
	
	/**
	 * Checks if the junk dimension consolidates Boolean flags into a single dimension to reduce complexity.
	 * Returns true if the dimension consists of Boolean flags.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBooleanFlag() : bool;
	
	/**
	 * Retrieves the current junk dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionJunk
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionJunk() : ?DimensionJunk;
	
	/**
	 * Sets the junk dimension type and returns the instance for method chaining.
	 *
	 * @param    DimensionJunk    $value
	 *
	 * @return DimensionJunkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionJunk(DimensionJunk $value) : DimensionJunkInterface;
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Junk;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * <h6>Junk Dimension trait.</h6>
 * Junk dimensions consolidate multiple low-cardinality attributes,
 * such as Boolean flags or small categories, into a single dimension.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionJunk {
	
	/**
	 * Junk dimension type.
	 *
	 * @var DimensionJunk
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionJunk $_dimensionJunk;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isBooleanFlag() : bool {
		// Return true if the dimension consolidates Boolean flags.
		if(! empty($this->_dimensionJunk)):
			return ( $this->getDimensionJunk()->value === DimensionJunk::BOOLEAN_FLAGS->value );
		endif;
		
		//
		throw new BadMethodCallException('Junk dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionJunk
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionJunk() : ?DimensionJunk {
		return $this->_dimensionJunk ?? NULL;
	}
	
	/**
	 * @param    DimensionJunk    $value
	 *
	 * @return DimensionJunkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionJunk(DimensionJunk $value) : DimensionJunkInterface {
		//
		$this->_dimensionJunk = $value;
		
		//
		return $this;
	}
}
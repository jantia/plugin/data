<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Junk;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Junk Dimensions</h6><br>
 * <b>Purpose:</b> Handle small, miscellaneous, or low-cardinality attributes that don’t fit into a standard
 * dimension table. Instead of creating many small dimensions, attributes are grouped into a "junk" dimension.<br><br>
 * <b>CQRS Use:</b> The Command side might insert or update records with these attributes, and the Query side
 * would use this dimension to group and filter data based on those attributes. Junk dimensions reduce the
 * complexity of queries.<br><br>
 * <b>Example:</b> Boolean flags, statuses, or other low-variability fields.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionJunk: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * A junk dimension that consolidates multiple Boolean flags into a single dimension to reduce complexity.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case BOOLEAN_FLAGS = 0;
	
	/**
	 * A junk dimension that holds various status attributes, grouping them together to avoid multiple dimensions for each status.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case STATUS_ATTRIBUTES = 1;
	
	/**
	 * A junk dimension that consolidates low-cardinality categorical data, such as small sets of descriptive labels.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case CATEGORICAL_DATA = 2;
	
	/**
	 * A junk dimension that combines multiple small attributes that don’t belong to other dimensions and don’t justify separate tables.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case MISC_ATTRIBUTES = 3;
	
	/**
	 * A junk dimension that is used to handle various indicators or flags that are used for reporting or filtering but don’t fit into other dimensions.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case INDICATORS = 4;
}
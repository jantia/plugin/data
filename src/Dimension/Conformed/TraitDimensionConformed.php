<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Conformed;

//
use Jantia\Plugin\Data\Exception\BadMethodCallException;

/**
 * <h6>Conformed Dimension trait.</h6>
 * Conformed dimensions are shared across multiple fact tables or data marts,
 * ensuring consistent reference data across systems.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait TraitDimensionConformed {
	
	/**
	 * Conformed dimension type.
	 *
	 * @var DimensionConformed
	 * @since   3.0.0 First time introduced.
	 */
	private readonly DimensionConformed $_dimensionConformed;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isSharedAcrossSystems() : bool {
		// Return true if dimension type is shared across multiple systems.
		if(! empty($this->_dimensionConformed)):
			return ( $this->getDimensionConformed()->value === DimensionConformed::SHARED->value );
		endif;
		
		// Throw exception if dimension type is not set.
		throw new BadMethodCallException('Conformed dimension type is not set.');
	}
	
	/**
	 * @return null|DimensionConformed
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionConformed() : ?DimensionConformed {
		return $this->_dimensionConformed ?? NULL;
	}
	
	/**
	 * @param    DimensionConformed    $value
	 *
	 * @return DimensionConformedInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionConformed(DimensionConformed $value) : DimensionConformedInterface {
		//
		$this->_dimensionConformed = $value;
		
		//
		return $this;
	}
}
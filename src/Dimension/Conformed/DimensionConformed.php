<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Conformed;

//
use Tiat\Standard\DataModel\InterfaceEnumInt;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * <h6>Conformed Dimensions</h6><br>
 * <b>Purpose:</b> Dimensions that are consistent and shared across different fact tables or subject areas. This
 * ensures consistency across different data marts or systems.<br><br>
 * <b>CQRS Use:</b> In a CQRS system, Command operations would modify these shared dimensions in a way that all
 * systems use the same consistent data, while Query operations pull from a single source of truth.<br><br>
 * <b>Example:</b> A shared customer or product dimension across multiple systems.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum DimensionConformed: int implements InterfaceEnumInt {
	
	//
	use TraitEnum;
	
	/**
	 * The dimension is consistent across all fact tables and data marts, ensuring that all systems use the same reference data.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case SHARED = 0;
	
	/**
	 * The dimension is used only by one specific fact table or subject area, meaning it is not conformed across systems.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case NON_CONFORMED = 1;
	
	/**
	 * A dimension that is partially conformed, used by more than one system but not all systems. It may vary in structure or content.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case PARTIALLY_CONFORMED = 2;
	
	/**
	 * A role-playing dimension that is used in different contexts (e.g., date used as order date, shipping date).
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case ROLE_PLAYING = 3;
	
	/**
	 * The dimension is aggregated or summarized for reporting purposes, typically used in performance-oriented systems.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case AGGREGATED = 4;
	
	/**
	 * A dimension that combines features of shared and role-playing dimensions, used across multiple systems and in different contexts.
	 *
	 * @var int
	 * @since   3.0.0 First time introduced.
	 */
	case HYBRID = 5;
}
<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Data
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Data\Dimension\Conformed;

/**
 * <h6>Conformed Dimension trait.</h6>
 * Conformed dimensions are shared across multiple fact tables or data marts,
 * ensuring consistent reference data across systems.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DimensionConformedInterface {
	
	/**
	 * Checks if the conformed dimension is shared across multiple fact tables or systems. Returns true if the
	 * dimension is consistent and used across different areas of the data warehouse, ensuring uniform reference data.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isSharedAcrossSystems() : bool;
	
	/**
	 * Returns the current conformed dimension type or null if the dimension type is not set.
	 *
	 * @return null|DimensionConformed
	 * @since   3.0.0 First time introduced.
	 */
	public function getDimensionConformed() : ?DimensionConformed;
	
	/**
	 * Sets the conformed dimension type and returns the updated instance of the class for method chaining.
	 *
	 * @param    DimensionConformed    $value
	 *
	 * @return DimensionConformedInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setDimensionConformed(DimensionConformed $value) : DimensionConformedInterface;
}